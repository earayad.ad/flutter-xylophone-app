import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

void main() => runApp(XylophoneApp());

// generate an interface that has a name and a color
class XylophoneButton {
  final String name;
  final Color color;

  XylophoneButton(this.name, this.color);
}

class XylophoneApp extends StatelessWidget {
  // generate a map with 7 color variable and soundNumber variable from 1 to 7
  final Map<int, XylophoneButton> colorMap = {
    1: XylophoneButton('note1.wav', Colors.red),
    2: XylophoneButton('note2.wav', Colors.orange),
    3: XylophoneButton('note3.wav', Colors.yellow),
    4: XylophoneButton('note4.wav', Colors.green),
    5: XylophoneButton('note5.wav', Colors.teal),
    6: XylophoneButton('note6.wav', Colors.blue),
    7: XylophoneButton('note7.wav', Colors.purple),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: colorMap.keys.map(
                  (sound) {
                    return Expanded(
                      child: TextButton(
                        style: TextButton.styleFrom(
                          backgroundColor: colorMap[sound].color,
                        ),
                        onPressed: () {
                          var player = AudioPlayer();
                          player.play(AssetSource(colorMap[sound].name));
                        }
                      ),
                    );
                  }
              ).toList()
            )
          ),
        ),
      ),
    );
  }
}
